<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchRequest;
use App\Trip;
use App\User;

class SearchController extends Controller
{
    public function search(SearchRequest $request)
    {
        $q = mb_strtolower($request->get('q'));
        $user_id = auth()->user()->id;

        $trips = Trip::notBelongingTo($user_id)->whereNotNull('ended_at')->where('private', 0)->where(function ($query) use ($q) {
            return $query->whereRaw('LOWER(location) LIKE ?', ['%'.$q.'%'])
            ->orWhereRaw('LOWER(location) LIKE ?', ['%'.$q.'%']);
        })->get()->each->append('thumbnail_id');

        $users = User::otherThan($user_id)->where('name', 'like', '%'.$q.'%')->get();

        return response()->json(['trips' => $trips, 'users' => $users]);
    }
}
