<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;
use Str;
use Storage;
use Hash;
use App\Token;
use App\User;
use App\Plan;
use App\Http\Requests\Auth\RegisterRequest;
use App\Http\Requests\Auth\LoginRequest;
use App\Http\Requests\Auth\ForgotPasswordRequest;
use Illuminate\Http\Request;

class AuthController extends \App\Http\Controllers\Controller
{
    /**
     * Creates a new user based on request and returns his new token.
     */
    public function register(RegisterRequest $request)
    {
        $data = getPayload();

        $user = new User();
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->password = Hash::make($data['password']);
        $user->verification_token = Str::random(50);
        $user->plan_id = Plan::where('name', 'free')->first()->id;
        $user->save();

        Storage::makeDirectory($user->id);

        $user->sendVerificationEmail();

        return response()->json(['message' => 'User created successfully. Please check your email account.', 'user' => ['id' => $user->id]], 201);
    }

    /**
     * Verifies email address for a user.
     */
    public function verifyEmail($token)
    {
        $user = User::where('verification_token', $token)->firstOrFail();

        if (Carbon::now()->diffInHours($user->updated_at) > 24) {
            $user->verification_token = Str::random(50);
            $user->save();

            $user->sendVerificationEmail();

            return response('This token has expired. We have sent a new link to your email address.');
        }

        $user->verify();

        return view('redirectToApp')
            ->with(['message' => 'EmailVerified/'.$user->id.'/'.Token::createForUser($user), 'fallbackUrl' => action('WebController@emailVerified')]);
    }

    /**
     * Receives an email and password and returns a new user token and his id.
     */
    public function login(LoginRequest $request)
    {
        $data = getPayload();

        $user = User::where('email', $data['email'])->first();

        if (!Hash::check($data['password'], $user->password)) {
            return response()->json(['message' => 'Password is incorrect'], 403);
        }

        if (!$user->hasVerifiedEmail()) {
            $user->sendVerificationEmail();

            return response()->json(['message' => 'Please verify your email address'], 403);
        }

        return response()->json(['message' => 'User logged in successfully', 'user' => ['id' => $user->id, 'token' => Token::createForUser($user), 'data' => $user]]);
    }

    /**
     * Receives a user token and id and removes the token from de database.
     */
    public function logout()
    {
        $data = getPayLoad();
        $token = request()->user->tokens()->where('value', hash('sha256', explode('.', request()->bearerToken())[1]))->firstOrFail();

        $token->delete();

        return response()->json(['message' => 'User logged out successfully']);
    }

    /**
     * Receives a user's email and sends a reset email.
     */
    public function forgotPassword(ForgotPasswordRequest $request)
    {
        $data = getPayload();

        $user = User::where('email', $data['email'])->firstOrFail();

        if (!$user->hasVerifiedEmail()) {
            return response()->json(['message' => 'Please verify your email address'], 403);
        }

        $token = $user->createPasswordResetToken();

        $user->sendResetPasswordEmail($token);

        return response()->json(['message' => 'A password reset link has been sent to your email address']);
    }
}
