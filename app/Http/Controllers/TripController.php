<?php

namespace App\Http\Controllers;

use Gate;
use Storage;
use App\Trip;
use App\Http\Requests\TripRequest;

class TripController extends Controller
{
    public function new(TripRequest $request)
    {
        if (request()->user->hasActiveTrip()) {
            return response()->json(['message' => 'You may only have one active trip at a time.'], 403);
        }

        if (request()->user->trips->count() >= request()->user->plan->allowed_trips) {
            return response()->json(['message' => 'You have exceeded the allowed limit of '.request()->user->plan->allowed_trips.' trips.'], 403);
        }

        $data = getPayload();

        $trip = Trip::createForUser(request()->user, $data['trip']);

        Storage::makeDirectory(request()->user->id.'/'.$trip->id);

        return response()->json(['message' => 'Trip successfully created.', 'trip' => $trip, 'user' => request()->user()], 201);
    }

    public function updateActiveTrip(TripRequest $request)
    {
        $trip = request()->user->hasActiveTrip();

        if (!$trip) {
            return response()->json(['message' => 'You don\'t have an active trip.'], 403);
        }

        $data = getPayload();

        $trip->name = $data['trip']['name'];
        $trip->location = $data['trip']['location'];

        $trip->save();

        return response()->json(['message' => 'Trip successfully updated.', 'trip' => $trip, 'user' => request()->user()], 200);
    }

    public function show(Trip $trip)
    {
        Gate::authorize('view-trip', $trip);

        // sort resources by taken_at date in desc order

        $resources = $trip->getLatestResources();

        return response()->json(['trip' => Trip::with('user')->find($trip->id), 'resources' => $resources->values()->all()]);
    }

    public function togglePublic(Trip $trip)
    {
        Gate::authorize('manage-trip', $trip);

        $trip->private = !$trip->private;

        $trip->save();

        return response()->json(['message' => 'Trip updated']);
    }

    public function suggestions()
    {
        $user_id = auth()->user()->id;

        $trips = Trip::notBelongingTo($user_id)->whereNotNull('ended_at')->where('private', 0)
            ->latest()->limit(6)->get()->each->append('thumbnail_id');

        return response()->json(['trips' => $trips]);
    }

    public function getPin(Trip $trip)
    {
        Gate::authorize('manage-trip', $trip);

        if (!$trip->private) {
            return response()->json(['message' => 'Cannot get pin for a public trip'], 422);
        }

        $pin = (string) random_int(100000, 999999);
        $trip->pin = bcrypt($pin);
        $trip->save();

        return response()->json(['pin' => $pin]);
    }

    public function delete(Trip $trip)
    {
        Gate::authorize('manage-trip', $trip);

        // clear the S3 storage

        Storage::deleteDirectory(request()->user->id.'/'.$trip->id);

        // clear the database

        $trip->delete();

        return response()->json(['message' => 'Trip successfully deleted.', 'user' => request()->user()], 200);
    }
}
