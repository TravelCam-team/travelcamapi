<?php

namespace App\Http\Controllers;

use Hash;
use Carbon\Carbon;
use App\PasswordResetToken;
use App\Http\Requests\Auth\ResetPasswordRequest;

class ResetPasswordController extends Controller
{
    public function getResetPassword($token)
    {
        return view('web.resetpassword')->with('token', $token);
    }

    /**
     * Receives a user's reset password token and the new password. Resets the password to that new one.
     */
    public function postResetPassword(ResetPasswordRequest $request)
    {
        $token = $request->input('token');
        $token = PasswordResetToken::where('token', hash('sha256', $token))->firstOrFail();

        if (Carbon::now()->diffInHours($token->updated_at) > 24) {
            $token->delete();

            return response('This token has expired. Please require another password reset');
        }

        $user = $token->user()->first();
        $user->password = Hash::make($request->input('password'));

        $token->delete();
        $user->save();

        return view('redirectToApp')->with(['message' => 'PasswordReset/0/0', 'fallbackUrl' => action('WebController@passwordWasResetted')]);
    }
}
