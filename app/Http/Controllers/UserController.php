<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\DescriptionRequest;
use App\Http\Requests\ProfilePictureRequest;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function authorizeUser()
    {
        return response()->json(request()->user);
    }

    public function show(User $user)
    {
        $user = User::with(['trips' => function ($query) {
            $query->whereNotNull('ended_at')->where('private', 0)->latest();
        }])->find($user->id)->makeHidden(['has_active_trip', 'ended_trips']);

        return response()->json($user);
    }

    public function endActiveTrip()
    {
        if ($trip = request()->user->hasActiveTrip()) {
            $trip->end();

            return response()->json(['message' => 'Trip ended.', 'user' => request()->user]);
        }

        return response()->json(['message' => 'No active trips for current user.'], 403);
    }

    public function setProfilePicture(ProfilePictureRequest $request)
    {
        request()->user->setProfilePicture($request);

        return response()->json(['message' => 'Profile picture added', 'user' => request()->user], 201);
    }

    public function setDescription(DescriptionRequest $request)
    {
        $user = request()->user;

        $user->description = $request->description;
        $user->save();

        return response()->json(['message' => 'Description successfully changed', 'user' => request()->user], 201);
    }
}
