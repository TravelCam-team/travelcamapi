<?php

namespace App\Http\Controllers;

use Cache;
use Hash;
use Storage;
use App\Diary;
use App\Trip;
use App\Photo;
use App\Http\Requests\PinRequest;

class WebController extends Controller
{
    public function home()
    {
        $trips = Cache::remember('featured_trips', 86400, function () {
            return Trip::get(['id', 'name', 'location'])->find([11, 39, 40, 43, 46, 47]);
        });

        return view('web/home', ['trips' => $trips]);
    }

    public function privacyPolicy()
    {
        return view('web/privacyPolicy');
    }

    public function showTrip(Trip $trip)
    {
        $resources = $trip->getLatestResources();

        return view('web/showTrip', ['trip' => $trip, 'resources' => $resources->paginate(48), 'user' => $trip->user]);
    }

    public function showDiaryBody(Diary $diary)
    {
        return '<div style="overflow-y:auto;max-height:100%;">'.e($diary->body).'</div>';
    }

    public function downloadAll(Trip $trip)
    {
        $zipname = storage_path('app/trip'.$trip->id.microtime().'.zip');
        $zip = new \ZipArchive();
        $zip->open($zipname, \ZipArchive::CREATE);

        foreach ($trip->photos as $photo) {
            $zip->addFromString($photo->name, Storage::get($photo->getPath()));
        }

        $i = 0;
        foreach ($trip->diaries as $diary) {
            $zip->addFromString('diary'.$i.'.txt', 'Title: '.$diary->title.PHP_EOL.PHP_EOL.'Body: '.PHP_EOL.$diary->body);

            ++$i;
        }

        $zip->close();

        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename=trip.zip');
        header('Content-Length: '.filesize($zipname));
        readfile($zipname);

        unlink($zipname);
    }

    public function downloadPhotos(Trip $trip)
    {
        $zipname = storage_path('app/trip'.$trip->id.microtime().'.zip');
        $zip = new \ZipArchive();
        $zip->open($zipname, \ZipArchive::CREATE);

        foreach ($trip->photos as $photo) {
            $zip->addFromString($photo->name, Storage::get($photo->getPath()));
        }

        $zip->close();

        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename=trip.zip');
        header('Content-Length: '.filesize($zipname));
        readfile($zipname);

        unlink($zipname);
    }

    public function enterPin(Trip $trip)
    {
        return view('web/enterPin', ['trip' => $trip]);
    }

    public function authenticatePin(PinRequest $request)
    {
        $trip = Trip::findOrFail($request->tripId);

        if (!Hash::check($request->pin, $trip->pin)) {
            return back()->with('error', 'This PIN is invalid.');
        }

        session(['trip'.$trip->id => true]);

        return redirect()->action('WebController@showTrip', ['trip' => $trip]);
    }

    public function showPhoto(Photo $photo)
    {
        return response(Storage::get($photo->getPath()))->header('Content-type', 'image/jpeg');
    }

    public function showThumbnail(Photo $photo)
    {
        return response($photo->thumbnail)->header('Content-type', 'image/jpeg');
    }

    public function emailVerified()
    {
        return view('web/emailVerified');
    }

    public function passwordWasResetted()
    {
        return view('web/passwordWasResetted');
    }
}
