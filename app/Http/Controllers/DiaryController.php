<?php

namespace App\Http\Controllers;

use App\Http\Requests\DiaryRequest;
use App\Diary;
use App\Trip;

class DiaryController extends Controller
{
    public function new(Trip $trip, DiaryRequest $request)
    {
        $diary = Diary::createForTrip($trip, $request);

        return response()->json(['message' => 'Diary entry added', 'diary' => $diary, 'user' => auth()->user()], 201);
    }
}
