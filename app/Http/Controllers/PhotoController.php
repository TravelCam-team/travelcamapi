<?php

namespace App\Http\Controllers;

use Image;
use Gate;
use Storage;
use App\Trip;
use App\Photo;
use Illuminate\Http\File;
use App\Http\Requests\PhotoRequest;

class PhotoController extends Controller
{
    public function new(Trip $trip, PhotoRequest $request)
    {
        if ($trip->user->plan->allowed_photos_per_trip <= $trip->photos->count()) {
            return response()->json(['message' => 'Exceeded number of allowed photos per trip.'], 403);
        }

        $thumbnail = Image::make($request->file('photo'))->fit(250, 250)->encode('jpg', 100);
        $image = Image::make($request->file('photo'))->encode('jpg', 100);

        $folder = auth()->user()->id.'/'.$trip->id;
        $path = Storage::putFile($folder, new File($image->dirname.'/'.$image->basename), 'private');

        $photo = Photo::createForTrip($trip, basename($path), $thumbnail, $request->taken_at);

        return response()->json(['message' => 'Photo '.$photo->getPath().' added', 'photo' => $photo, 'user' => auth()->user()], 201);
    }

    public function show(Photo $photo)
    {
        Gate::authorize('view-photo', $photo);

        return response(Storage::get($photo->getPath()))->header('Content-type', 'image/jpeg');
    }

    public function showThumbnail(Photo $photo)
    {
        Gate::authorize('view-photo', $photo);

        return response($photo->thumbnail)->header('Content-type', 'image/jpeg');
    }
}
