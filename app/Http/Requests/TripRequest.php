<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TripRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'trip.location' => 'required|string|max:255',
            'trip.name' => 'required|string|max:255',
        ];
    }

    public function attributes()
    {
        return [
            'trip.location' => 'trip\'s location',
            'trip.name' => 'trip\'s name',
        ];
    }
}
