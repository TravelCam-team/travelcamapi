<?php

namespace App\Http\Middleware;

use Closure;

class PinAuthenticate
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $trip = $request->route('trip');
        $diary = $request->route('diary');
        $photo = $request->route('photo');

        if ($diary) {
            $trip = $diary->trip;
        } elseif ($photo) {
            $trip = $photo->trip;
        }

        if (!$trip->private) {
            return $next($request);
        }

        if (!$request->session()->has('trip'.$trip->id)) {
            return redirect()->action('WebController@enterPin', ['trip' => $trip->id])
                ->with('error', 'You must have a PIN to access this private resource');
        }

        return $next($request);
    }
}
