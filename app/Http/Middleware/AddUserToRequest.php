<?php

namespace App\Http\Middleware;

use App\Token;
use Closure;

class AddUserToRequest
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->user = null;

        if ($request->bearerToken()) {
            $data = explode('.', $request->bearerToken()); // id, token of user

            $token = hash('sha256', $data[1]);
            $query = Token::where('user_id', $data[0])->where('value', $token);

            if ($query->count() == 1) {
                if (!$query->first()->user->hasVerifiedEmail()) {
                    return response()->json(['message' => 'Email address not verified'], 403);
                }

                auth()->setUser($query->first()->user);
                $request->user = $query->first()->user;
            }
        }

        return $next($request);
    }
}
