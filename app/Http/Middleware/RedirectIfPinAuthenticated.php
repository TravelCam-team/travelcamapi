<?php

namespace App\Http\Middleware;

use App\Trip;
use Closure;

class RedirectIfPinAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $trip = $request->route('trip');

        if (!$trip && $request->tripId) {
            $trip = Trip::findOrFail($request->tripId);
        }

        if (!$trip->private) {
            return redirect()->action('WebController@showTrip', ['trip' => $trip->id]);
        }

        if ($request->session()->has('trip'.$trip->id)) {
            return redirect()->action('WebController@showTrip', ['trip' => $trip->id]);
        }

        return $next($request);
    }
}
