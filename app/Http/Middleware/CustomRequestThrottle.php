<?php

namespace App\Http\Middleware;

class CustomRequestThrottle extends \Illuminate\Routing\Middleware\ThrottleRequests
{
    protected function resolveRequestSignature($request)
    {
        if ($user = $request->user) {
            return sha1($user->id);
        }

        if ($route = $request->route()) {
            return sha1($route->getDomain().'|'.$request->ip());
        }

        throw new RuntimeException('Unable to generate the request signature. Route unavailable.');
    }
}
