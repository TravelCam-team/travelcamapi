<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    public function user()
    {
        return $this->hasMany('App\User');
    }
}
