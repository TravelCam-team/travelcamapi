<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasswordResetToken extends Model
{
    protected $table = 'password_resets';

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
