<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $hidden = [
        'user_id',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function photos()
    {
        return $this->hasMany('App\Photo');
    }

    public function diaries()
    {
        return $this->hasMany('App\Diary');
    }

    public function end()
    {
        $this->ended_at = Carbon::now();

        $this->save();
    }

    public function getThumbnailIdAttribute()
    {
        if ($this->photos->count()) {
            return $this->photos->first()->id;
        }

        return null;
    }

    public function scopeNotBelongingTo($query, $user_id)
    {
        return $query->where('user_id', '!=', $user_id);
    }

    public static function createForUser($user, $data)
    {
        $trip = new Trip();
        $trip->location = $data['location'];

        $trip->name = $data['name'];

        $trip->user()->associate($user);
        $trip->save();

        return $trip;
    }

    public function getLatestResources()
    {
        return $this->photos->concat($this->diaries)->sort('latestDateComparator');
    }
}
