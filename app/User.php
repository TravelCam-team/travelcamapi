<?php

namespace App;

use Storage;
use Str;
use Carbon\Carbon;
use Mail;
use App\Mail\VerificationEmail;
use App\Mail\ResetPasswordEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'email_verified_at', 'verification_token',
    ];

    protected $dates = ['email_verified_at'];

    protected $appends = ['has_active_trip', 'ended_trips', 'allowed_photos_per_trip', 'has_ads'];

    public function tokens()
    {
        return $this->hasMany('App\Token');
    }

    public function passwordResetTokens()
    {
        return $this->hasMany('App\PasswordResetToken');
    }

    public function trips()
    {
        return $this->hasMany('App\Trip');
    }

    public function plan()
    {
        return $this->belongsTo('App\Plan');
    }

    public function hasVerifiedEmail()
    {
        return $this->email_verified_at != null;
    }

    public function scopeOtherThan($query, $user_id)
    {
        return $query->where('id', '!=', $user_id);
    }

    public function verify()
    {
        $this->email_verified_at = Carbon::now();
        $this->verification_token = null;
        $this->save();
    }

    public function sendVerificationEmail()
    {
        Mail::to($this)->send(new VerificationEmail($this));
    }

    public function createPasswordResetToken()
    {
        $token = new PasswordResetToken();
        $token->token = hash('sha256', $unhashed = Str::random(50));
        $token->user()->associate($this);
        $token->save();

        return $unhashed;
    }

    public function setProfilePicture($request)
    {
        $path = Storage::disk('profile_pictures')->putFile('/', $request->file('photo'), 'public');

        if ($this->profile_picture) {
            Storage::disk('profile_pictures')->delete($this->profile_picture);
        }

        $this->profile_picture = $path;
        $this->profile_picture_permalink = Storage::disk('profile_pictures')->url($path);
        $this->save();
    }

    public function sendResetPasswordEmail($token)
    {
        Mail::to($this)->send(new ResetPasswordEmail($this, $token));
    }

    public function hasActiveTrip()
    {
        $trips = $this->trips()->with(['photos' => function ($query) {
            $query->orderBy('created_at', 'desc');
        }])->where('ended_at', null);

        if ($trips->count() == 1) {
            return $trips->first();
        }

        return false;
    }

    public function getHasActiveTripAttribute()
    {
        return $this->hasActiveTrip();
    }

    public function getEndedTripsAttribute()
    {
        return $this->trips()->whereNotNull('ended_at')->latest()->get();
    }

    public function getAllowedPhotosPerTripAttribute()
    {
        return $this->plan->allowed_photos_per_trip;
    }

    public function getHasAdsAttribute()
    {
        return $this->plan->has_ads;
    }
}
