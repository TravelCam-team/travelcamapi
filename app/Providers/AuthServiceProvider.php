<?php

namespace App\Providers;

use App\User;
use App\Trip;
use App\Photo;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('view-photo', function (User $user, Photo $photo) {
            return !$photo->trip->private || $user->id == $photo->trip->user->id;
        });

        Gate::define('manage-trip', function (User $user, Trip $trip) {
            return $user->id == $trip->user->id;
        });

        Gate::define('view-trip', function (User $user, Trip $trip) {
            return !$trip->private || $user->id == $trip->user->id;
        });
    }
}
