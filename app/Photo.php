<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{
    public $timestamps = ['taken_at'];

    public $hidden = ['thumbnail'];

    public function getPath()
    {
        return $this->trip->user->id.'/'.$this->trip->id.'/'.$this->name;
    }

    public function getThumbnailPath()
    {
        return $this->trip->user->id.'/'.$this->trip->id.'/'.$this->thumbnail;
    }

    public function trip()
    {
        return $this->belongsTo('App\Trip');
    }

    public static function createForTrip($trip, $path, $thumbnail, $taken_at)
    {
        $taken_at = Carbon::parse($taken_at)->setTimezone('UTC');

        $photo = new Photo();
        $photo->name = $path;
        $photo->thumbnail = $thumbnail;
        $photo->taken_at = $taken_at;

        $photo->trip()->associate($trip);
        $photo->save();

        return $photo;
    }
}
