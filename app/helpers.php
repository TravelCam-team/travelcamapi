<?php

function getPayload()
{
    return json_decode(request()->getContent(), true);
}

function latestDateComparator($item1, $item2)
{
    $dateA = \Carbon\Carbon::parse($item1->taken_at);
    $dateB = \Carbon\Carbon::parse($item2->taken_at);

    if ($dateA->lt($dateB)) {
        return 1;
    }

    if ($dateA->gt($dateB)) {
        return -1;
    }

    return 0;
}
