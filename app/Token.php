<?php

namespace App;

use Str;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function createForUser($user)
    {
        $token = new Token;
        $token->value =  hash('sha256', $unhashed = Str::random(80));
        $token->user()->associate($user);
        $token->save();

        return $unhashed;
    }
}
