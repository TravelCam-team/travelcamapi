<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Diary extends Model
{
    public $timestamps = ['taken_at'];

    public function trip()
    {
        return $this->belongsTo('App\Trip');
    }

    public static function createForTrip($trip, $request)
    {
        $diary = new Diary();

        $diary->title = $request->title;
        $diary->body = $request->body;
        $diary->taken_at = Carbon::parse($request->taken_at)->setTimezone('UTC');
        $diary->trip()->associate($trip);
        $diary->save();

        return $diary;
    }
}
