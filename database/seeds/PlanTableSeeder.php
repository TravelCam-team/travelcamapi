<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class PlanTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run()
    {
        DB::table('plans')->insert([
            'name' => 'free',
            'allowed_trips' => 5,
            'allowed_photos_per_trip' => 50,
        ]);

        DB::table('plans')->insert([
            'name' => 'pro',
            'allowed_trips' => 9999,
            'allowed_photos_per_trip' => 9999,
            'has_ads' => 0,
        ]);
    }
}
