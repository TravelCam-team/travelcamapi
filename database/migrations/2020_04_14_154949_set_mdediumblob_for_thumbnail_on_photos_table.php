<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Migrations\Migration;

class SetMdediumblobForThumbnailOnPhotosTable extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        if (app()->environment() !== 'testing') {
            DB::statement('ALTER TABLE `photos` MODIFY `thumbnail` MEDIUMBLOB;');
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        if (app()->environment() !== 'testing') {
            DB::statement('ALTER TABLE `photos` MODIFY `thumbnail` BLOB;');
        }
    }
}
