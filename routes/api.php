<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::namespace('Auth')->group(function () {
    Route::post('/register', 'AuthController@register');
    Route::post('/login', 'AuthController@login');
    Route::post('/password/forgot', 'AuthController@forgotPassword');
    Route::middleware('auth')->post('/logout', 'AuthController@logout');
});

Route::middleware('auth')->group(function () {
    Route::get('/user/authorize', 'UserController@authorizeUser');
    Route::get('/user/show/{user}', 'UserController@show');
    Route::post('/user/endActiveTrip', 'UserController@endActiveTrip');
    Route::post('/user/setProfilePicture', 'UserController@setProfilePicture');
    Route::post('/user/setDescription', 'UserController@setDescription');

    Route::post('/trip/new', 'TripController@new');
    Route::get('/trip/suggestions', 'TripController@suggestions');
    Route::get('/trip/show/{trip}', 'TripController@show');
    Route::post('trip/updateActiveTrip', 'TripController@updateActiveTrip');
    Route::post('trip/{trip}/delete', 'TripController@delete');
    Route::post('trip/{trip}/togglePublic', 'TripController@togglePublic');
    Route::post('trip/{trip}/getPin', 'TripController@getPin');

    Route::post('/photo/{trip}/new', 'PhotoController@new');
    Route::get('/photo/showThumbnail/{photo}', 'PhotoController@showThumbnail');
    Route::get('/photo/show/{photo}', 'PhotoController@show');

    Route::post('/diary/{trip}/new', 'DiaryController@new');

    Route::get('/search', 'SearchController@search');
});
