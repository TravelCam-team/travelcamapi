<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'WebController@home');

Route::get('/privacyPolicy', 'WebController@privacyPolicy');

Route::namespace('Auth')->group(function () {
    Route::get('/verify/{token}', 'AuthController@verifyEmail');
});

Route::get('/resetPassword/{token}', 'ResetPasswordController@getResetPassword');
Route::post('/resetPassword', 'ResetPasswordController@postResetPassword');

Route::middleware('guest.pin')->group(function () {
    Route::get('/enterPin/{trip}', 'WebController@enterPin');
    Route::post('/authenticatePin', 'WebController@authenticatePin');
});

Route::middleware('auth.pin')->group(function () {
    Route::get('/trip/{trip}', 'WebController@showTrip');
    Route::get('/diary/{diary}', 'WebController@showDiaryBody');
    Route::post('/trip/{trip}/downloadAll', 'WebController@downloadAll');
    Route::post('/trip/{trip}/downloadPhotos', 'WebController@downloadPhotos');

    Route::get('/photo/showThumbnail/{photo}', 'WebController@showThumbnail');
    Route::get('/photo/show/{photo}', 'WebController@showPhoto');
});

Route::get('/emailVerified', 'WebController@emailVerified');
Route::get('/passwordWasResetted', 'WebController@passwordWasResetted');
