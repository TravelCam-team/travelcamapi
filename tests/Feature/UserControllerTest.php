<?php

namespace Tests\Feature;

use Storage;
use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserContollerTest extends TestCase
{
    use RefreshDatabase;

    public function testEndActiveTrip()
    {
        $user = $this->makeUser();

        // end the active trip

        $trip = $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['location' => 'China', 'name' => 'Best trip ever'],
        ])->decodeResponseJson()['trip'];

        $this->assertEquals(User::find($user['id'])->hasActiveTrip()->id, $trip['id']);

        $this->withAuth($user)->json('post', '/api/user/endActiveTrip')->assertStatus(200)->assertJson(['message' => 'Trip ended.']);

        $this->assertEquals(User::find($user['id'])->hasActiveTrip(), false);

        // fail if no active trip

        $this->withAuth($user)->json('post', '/api/user/endActiveTrip')->assertStatus(403)->assertJson(['message' => 'No active trips for current user.']);
    }

    public function testSetProfilePicture()
    {
        Storage::fake('profile_pictures');

        $user = $this->makeUser();

        // test that user has the default profile picture

        $this->assertEquals(User::find($user['id'])->profile_picture_permalink, env('DEFAULT_PROFILE_PICTURE'));

        // test failed validation

        $file = UploadedFile::fake()->image('photo.bmp');

        $response = $this->withAuth($user)->json('post', '/api/user/setProfilePicture', [
            'photo' => $file,
        ]);

        $response->assertStatus(422);

        // test successful file upload

        $this->assertTrue(User::find($user['id'])->profile_picture == null);

        $file = UploadedFile::fake()->image('photo.jpg');

        $response = $this->withAuth($user)->json('post', '/api/user/setProfilePicture', [
            'photo' => $file,
        ]);

        $response->assertStatus(201);

        $this->assertTrue(User::find($user['id'])->profile_picture != null);
        Storage::disk('profile_pictures')->assertExists(User::find($user['id'])->profile_picture);

        // test deletion after new profile picture

        $this->assertTrue(User::find($user['id'])->profile_picture != null);
        $oldPhoto = User::find($user['id'])->profile_picture;

        $file = UploadedFile::fake()->image('photo.jpg');

        $response = $this->withAuth($user)->json('post', '/api/user/setProfilePicture', [
            'photo' => $file,
        ]);

        $response->assertStatus(201);

        $this->assertTrue(User::find($user['id'])->profile_picture != null);
        Storage::disk('profile_pictures')->assertExists(User::find($user['id'])->profile_picture);
        Storage::disk('profile_pictures')->assertMissing($oldPhoto);
    }

    public function testSetDescription()
    {
        $user = $this->makeUser();

        // test failed validation

        $response = $this->withAuth($user)->json('post', '/api/user/setDescription', [
            'description' => 124,
        ]);

        $response->assertStatus(422);

        // test successful setting of description

        $this->assertTrue(User::find($user['id'])->description == null);

        $response = $this->withAuth($user)->json('post', '/api/user/setDescription', [
            'description' => 'lala',
        ]);

        $response->assertStatus(201);

        $this->assertEquals(User::find($user['id'])->description, 'lala');
    }
}
