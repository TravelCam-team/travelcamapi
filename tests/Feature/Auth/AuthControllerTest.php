<?php

namespace Tests\Feature\Auth;

use Carbon\Carbon;
use Mail;
use Storage;
use App\User;
use App\PasswordResetToken;
use App\Mail\VerificationEmail;
use App\Mail\ResetPasswordEmail;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testRegister()
    {
        Storage::fake('s3');
        Mail::fake();

        // test that the user was created successfully, as well as his token

        $response = $this->json('post', '/api/register', [
            'name' => 'test1',
            'email' => 'test1@yahoo.com',
            'password' => 'password123456',
            'password_confirmation' => 'password123456',
        ]);

        $user = $response->decodeResponseJson()['user'];

        $response->assertStatus(201)->assertJson(['message' => 'User created successfully. Please check your email account.']);

        $this->assertDatabaseHas('users', [
            'name' => 'test1',
            'email' => 'test1@yahoo.com',
        ]);

        $this->assertEquals(User::where('name', 'test1')->first()->tokens->count(), 0);
        Storage::assertExists(User::where('name', 'test1')->first()->id);

        Mail::assertQueued(VerificationEmail::class, function ($mail) use ($user) {
            return $mail->user->id === $user['id'];
        });

        // test that the user is not created when validation fails

        Mail::fake();

        $response = $this->json('post', '/api/register', [
            'name' => 'test1',
            'email' => 'test1@yahoo.com',
            'password' => 'password123456',
            'password_confirmation' => 'password123456',
        ]);

        $response->assertStatus(422);

        $this->assertDatabaseHas('users', [
            'name' => 'test1',
            'email' => 'test1@yahoo.com',
        ]);

        $this->assertEquals(User::where('name', 'test1')->count(), 1);

        Mail::assertNothingQueued();
    }

    public function testVerifyEmail()
    {
        $this->json('post', '/api/register', [
            'name' => 'test1',
            'email' => 'test1@yahoo.com',
            'password' => 'password123456',
            'password_confirmation' => 'password123456',
        ]);

        // wrong token

        $this->get('/verify/adashjklhjkldasd')->assertStatus(404);

        // expired token
        Mail::fake();

        $user = User::where('name', 'test1')->first();
        $token = $user->verification_token;

        $newDate = $user->updated_at->add(2, 'day');
        Carbon::setTestNow($newDate);

        $this->get('/verify/'.$token)->assertStatus(200)->assertSee('This token has expired. We have sent a new link to your email address.');

        Mail::assertQueued(VerificationEmail::class, function ($mail) use ($user) {
            return $mail->user->id === $user['id'];
        });

        // valid token

        Mail::fake();

        $token = User::where('name', 'test1')->first()->verification_token;
        $this->get('/verify/'.$token)->assertStatus(200)->assertSee('Redirecting...');

        $response = $this->json('post', '/api/login', [
            'email' => 'test1@yahoo.com',
            'password' => 'password123456',
        ]);

        $response->assertStatus(200)->assertJson(['message' => 'User logged in successfully']);

        Mail::assertNothingQueued();

        $newDate = $newDate->sub(2, 'day');
        Carbon::setTestNow($newDate);
    }

    public function testLogin()
    {
        $response = $this->json('post', '/api/register', [
            'name' => 'test1',
            'email' => 'test1@yahoo.com',
            'password' => 'password123456',
            'password_confirmation' => 'password123456',
        ]);

        $user = $response->decodeResponseJson()['user'];

        // email not verified

        Mail::fake();
        $response = $this->json('post', '/api/login', [
            'email' => 'test1@yahoo.com',
            'password' => 'password123456',
        ]);

        Mail::assertQueued(VerificationEmail::class, function ($mail) use ($user) {
            return $mail->user->id === $user['id'];
        });

        $response->assertStatus(403)->assertJson(['message' => 'Please verify your email address']);

        // successful login

        User::find(1)->first()->verify();

        $response = $this->json('post', '/api/login', [
            'email' => 'test1@yahoo.com',
            'password' => 'password123456',
        ]);

        $response->assertStatus(200)->assertJson(['message' => 'User logged in successfully']);

        $user = User::where('name', 'test1')->first();
        $this->assertEquals($user->tokens->count(), 1);

        // wrong password

        $response = $this->json('post', '/api/login', [
            'email' => 'test1@yahoo.com',
            'password' => 'password12345',
        ]);

        $response->assertStatus(403)->assertJson(['message' => 'Password is incorrect']);
        $this->assertEquals($user->tokens->count(), 1);

        // wrong email

        $response = $this->json('post', '/api/login', [
            'email' => 'test1@yahoooooo.com',
            'password' => 'password123456',
        ]);

        $response->assertStatus(422);
    }

    public function testLogout()
    {
        $user = $this->makeUser();

        // test successful logout
        $this->assertEquals(User::find($user['id'])->first()->tokens->count(), 1);

        $response = $this->withAuth($user)->json('post', '/api/logout');

        $response->assertStatus(200)->assertJson(['message' => 'User logged out successfully']);
        $this->assertEquals(User::find($user['id'])->first()->tokens->count(), 0);
    }

    public function testForgotPassword()
    {
        $user = $this->makeUser();

        Mail::fake();

        $response = $this->json('post', '/api/password/forgot', ['email' => 'test1@yahoo.com']);

        $response->assertStatus(200)->assertJson([
            'message' => 'A password reset link has been sent to your email address',
        ]);

        $this->assertEquals(PasswordResetToken::where('user_id', $user['id'])->count(), 1);

        Mail::assertQueued(ResetPasswordEmail::class, function ($mail) use ($user) {
            return $mail->user->id === $user['id'];
        });
    }
}
