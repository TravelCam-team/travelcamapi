<?php

namespace Tests\Feature\Auth;

use Carbon\Carbon;
use App\User;
use App\PasswordResetToken;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ResetPasswordControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testPostResetPassword()
    {
        $this->makeUser();

        $user = User::where('email', 'test1@yahoo.com')->first();
        $unhashed = $user->createPasswordResetToken();

        // test wrong token

        $response = $this->post('/resetPassword', [
            'token' => 'blah',
            '_token' => csrf_token(),
            'password' => 'passsssssssss',
            'password_confirmation' => 'passsssssssss',
        ]);

        $response->assertStatus(404);

        // test that validation fails

        $response = $this->post('/resetPassword', [
            'token' => 'blah',
            '_token' => csrf_token(),
            'password' => 'passsssssssss',
            'password_confirmation' => 'p',
        ]);

        $response->assertStatus(302); // it redirects back

        // test expired token

        $token = PasswordResetToken::where('user_id', $user['id'])->first();

        $newDate = $token->updated_at->add(2, 'day');
        Carbon::setTestNow($newDate);

        $response = $this->post('/resetPassword', [
            'token' => $unhashed,
            '_token' => csrf_token(),
            'password' => 'passsssssssss',
            'password_confirmation' => 'passsssssssss',
        ]);

        $response->assertStatus(200)->assertSee('This token has expired. Please require another password reset');

        $this->assertEquals(User::where('email', 'test1@yahoo.com')->first()->passwordResetTokens->count(), 0);

        $newDate = $newDate->sub(2, 'day');
        Carbon::setTestNow($newDate);

        // test correct token

        $unhashed = $user->createPasswordResetToken();

        $response = $this->post('/resetPassword', [
            'token' => $unhashed,
            '_token' => csrf_token(),
            'password' => 'passsssssssss',
            'password_confirmation' => 'passsssssssss',
        ]);

        $response->assertStatus(200)->assertSee('Redirecting...');

        $this->assertEquals(User::where('email', 'test1@yahoo.com')->first()->passwordResetTokens->count(), 0);

        $response = $this->json('post', '/api/login', [
            'email' => 'test1@yahoo.com',
            'password' => 'passsssssssss',
        ]);

        $response->assertStatus(200)->assertJson(['message' => 'User logged in successfully']);
    }
}
