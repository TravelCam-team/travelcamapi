<?php

namespace Tests\Feature\Auth;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuthenticateMiddlewareTest extends TestCase
{
    use RefreshDatabase;

    public function testAuthRoute()
    {
        $user = $this->makeUser();

        // test that the user can't access authorized routes if email is not verified

        $model = User::where('id', $user['id'])->first();
        $model->email_verified_at = null;
        $model->save();

        $response = $this->withAuth($user)->json('get', '/api/user/authorize');

        $response->assertStatus(403)->assertJson(['message' => 'Email address not verified']);

        // test that the user can only access authorize route with a valid token and id

        $model->verify();
        $response = $this->withAuth($user)->json('get', '/api/user/authorize');

        $response->assertStatus(200);
        // test for invalid token

        $user['token'] = 'blahblah';

        $response = $this->withAuth($user)->json('get', '/api/user/authorize');

        $response->assertStatus(403)->assertJson(['message' => 'You must be logged in to access this resource.']);
    }
}
