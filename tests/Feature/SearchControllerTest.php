<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SearchControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testSearch()
    {
        $user = $this->makeUser('China');
        $user2 = $this->makeUser('zzzzzzzzzzzzz');

        $trip = $this->makeTrip($user, 'China', 'Trip1');
        $this->withAuth($user)->json('post', '/api/user/endActiveTrip');

        $trip2 = $this->makeTrip($user, 'England', 'Trip2');
        $this->withAuth($user)->json('post', '/api/user/endActiveTrip');

        $trip3 = $this->makeTrip($user, 'Active', 'Trip2');

        // test failed validation

        $response = $this->withAuth($user2)->json('get', '/api/search/?q=', []);
        $response->assertStatus(422);

        // test general use cases
        $response = $this->withAuth($user2)->json('get', '/api/search/?q=ngla', []);

        $response->assertStatus(200);
        $response = $response->decodeResponseJson();

        $this->assertEquals(count($response['trips']), 1);
        $this->assertEquals(count($response['users']), 0);
        $this->assertEquals($response['trips'][0]['location'], 'England');

        $response = $this->withAuth($user2)->json('get', '/api/search/?q=a', []);

        $response->assertStatus(200);
        $response = $response->decodeResponseJson();
        $this->assertEquals(count($response['trips']), 2);
        $this->assertEquals(count($response['users']), 1);

        // test case insensitivity
        $response = $this->withAuth($user2)->json('get', '/api/search/?q=england', []);
        $response->assertStatus(200);
        $response = $response->decodeResponseJson();
        $this->assertEquals(count($response['trips']), 1);
        $this->assertEquals(count($response['users']), 0);
        $this->assertEquals($response['trips'][0]['location'], 'England');
    }
}
