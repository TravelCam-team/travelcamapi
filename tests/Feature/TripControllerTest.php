<?php

namespace Tests\Feature;

use App\User;
use App\Trip;
use Storage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TripControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testNew()
    {
        Storage::fake('s3');

        $user = $this->makeUser();

        // fail validation

        $response = $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['name' => 'Best trip ever'],
        ]);

        $response->assertStatus(422);

        $this->assertEquals(User::find($user['id'])->first()->trips->count(), 0);

        // create trip successfully

        $response = $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['location' => 'China', 'name' => 'Best trip ever'],
        ]);

        $response->assertStatus(201)->assertJson(['message' => 'Trip successfully created.']);

        $trip = $response->decodeResponseJson()['trip'];

        $this->assertEquals(User::where('id', $user['id'])->first()->trips->count(), 1);

        $this->assertEquals($trip['name'], 'Best trip ever');
        $this->assertEquals($trip['location'], 'China');

        Storage::assertExists(User::where('name', 'test1')->first()->id.'/'.$trip['id']);

        // fail if already has an active trip

        $response = $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['location' => 'China', 'name' => 'Best trip ever'],
        ]);

        $response->assertStatus(403)->assertJson(['message' => 'You may only have one active trip at a time.']);
        $this->assertEquals(User::where('id', $user['id'])->first()->trips->count(), 1);

        $this->withAuth($user)->json('post', '/api/user/endActiveTrip', [
            ])->assertStatus(200);

        // fail if exceeded number of trips allowed in current plan
        $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['location' => 'China', 'name' => 'Best trip ever'],
        ])->assertStatus(201);
        $this->withAuth($user)->json('post', '/api/user/endActiveTrip', [
        ])->assertStatus(200);

        $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['location' => 'China', 'name' => 'Best trip ever'],
        ])->assertStatus(201);
        $this->withAuth($user)->json('post', '/api/user/endActiveTrip', [
        ])->assertStatus(200);

        $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['location' => 'China', 'name' => 'Best trip ever'],
        ])->assertStatus(201);
        $this->withAuth($user)->json('post', '/api/user/endActiveTrip', [
        ])->assertStatus(200);

        $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['location' => 'China', 'name' => 'Best trip ever'],
        ])->assertStatus(201);
        $this->withAuth($user)->json('post', '/api/user/endActiveTrip', [
        ])->assertStatus(200);

        $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['location' => 'China', 'name' => 'Best trip ever'],
        ])->assertStatus(403)->assertJson(['message' => 'You have exceeded the allowed limit of 5 trips.']);
    }

    public function testUpdateActiveTrip()
    {
        $user = $this->makeUser();
        $trip = $this->makeTrip($user);

        // successful update
        $this->withAuth($user)->json('post', '/api/trip/updateActiveTrip', [
            'trip' => ['location' => 'China2', 'name' => 'Best trip ever'],
        ])->assertStatus(200)->assertJson(['message' => 'Trip successfully updated.']);

        $this->assertEquals(Trip::where('id', $trip['id'])->first()->location, 'China2');

        // no active trip
        Trip::where('id', $trip['id'])->first()->end();

        $this->withAuth($user)->json('post', '/api/trip/updateActiveTrip', [
            'trip' => ['location' => 'China2', 'name' => 'Best trip ever'],
        ])->assertStatus(403)->assertJson(['message' => 'You don\'t have an active trip.']);
    }

    public function testShow()
    {
        $user = $this->makeUser();

        $trip = $this->makeTrip($user);

        $this->withAuth($user)->json('post', '/api/trip/'.$trip['id'].'/togglePublic');

        $response = $this->withAuth($user)->json('get', '/api/trip/show/'.$trip['id']);
        $response->assertStatus(200);

        // test that authorization fails for different user

        $user = $this->makeUser(2);

        $response = $this->withAuth($user)->json('get', '/api/trip/show/'.$trip['id']);

        $response->assertStatus(403);
    }

    public function testTogglePublic()
    {
        $user = $this->makeUser();

        $trip = $this->makeTrip($user);

        $response = $this->withAuth($user)->json('post', '/api/trip/'.$trip['id'].'/togglePublic');

        $response->assertStatus(200);

        $this->assertEquals(Trip::where('id', $trip['id'])->first()->private, 1);

        $response = $this->withAuth($user)->json('post', '/api/trip/'.$trip['id'].'/togglePublic');

        $response->assertStatus(200);

        $this->assertEquals(Trip::where('id', $trip['id'])->first()->private, 0);

        // test that authorization fails for different user

        $user = $this->makeUser(2);

        $response = $this->withAuth($user)->json('post', '/api/trip/'.$trip['id'].'/togglePublic');

        $response->assertStatus(403);
    }

    public function testDelete()
    {
        Storage::fake('s3');

        $user = $this->makeUser();

        // test that authorization fails for different user

        $trip = $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['location' => 'China', 'name' => 'Best trip ever'],
        ])->decodeResponseJson()['trip'];

        $user2 = $this->makeUser(2);

        $response = $this->withAuth($user2)->json('post', '/api/trip/'.$trip['id'].'/delete');

        $response->assertStatus(403);

        // delete trip successfully

        $response = $this->withAuth($user)->json('post', '/api/trip/'.$trip['id'].'/delete');

        $response->assertStatus(200)->assertJson(['message' => 'Trip successfully deleted.']);

        $this->assertEquals(User::where('id', $user['id'])->first()->trips->count(), 0);

        Storage::assertMissing(User::where('name', 'test1')->first()->id.'/'.$trip['id']);
    }
}
