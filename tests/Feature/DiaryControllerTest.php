<?php

namespace Tests\Feature;

use Carbon\Carbon;
use App\Trip;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class DiaryControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testNew()
    {
        $user = $this->makeUser();

        $trip = $this->makeTrip($user);

        // test failed validation

        $response = $this->withAuth($user)->json('post', '/api/diary/'.$trip['id'].'/new', [
            'title' => '1 dec 2019',
            'taken_at' => Carbon::now()->toIso8601String(),
        ]);

        $response->assertStatus(422);

        // test successful diary submission

        $date = Carbon::now();
        $response = $this->withAuth($user)->json('post', '/api/diary/'.$trip['id'].'/new', [
            'title' => '1 dec 2019',
            'body' => 'sadsadsadsadsadsadsadsada',
            'taken_at' => $date->toIso8601String(),
        ]);

        $response->assertStatus(201);

        $diary = $response->decodeResponseJson()['diary'];

        $this->assertEquals(Trip::find($trip['id'])->first()->diaries->count(), 1);
        $this->assertEquals(Trip::find($trip['id'])->first()->diaries->first()->title, $diary['title']);
        $this->assertEquals(Trip::find($trip['id'])->first()->diaries->first()->taken_at, $date->setTimezone('UTC'));
    }
}
