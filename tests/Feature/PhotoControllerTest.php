<?php

namespace Tests\Feature;

use Carbon\Carbon;
use App\Plan;
use App\Trip;
use Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PhotoControllerTest extends TestCase
{
    use RefreshDatabase;

    public function testNew()
    {
        Storage::fake('s3');

        $user = $this->makeUser();

        $trip = $this->makeTrip($user);

        // test failed validation

        $file = UploadedFile::fake()->image('photo.bmp');

        $response = $this->withAuth($user)->json('post', '/api/photo/'.$trip['id'].'/new', [
            'photo' => $file,
        ]);

        $response->assertStatus(422);

        // test successful file upload

        $file = UploadedFile::fake()->image('photo.jpg');

        $date = Carbon::now();
        $response = $this->withAuth($user)->json('post', '/api/photo/'.$trip['id'].'/new', [
            'photo' => $file,
            'taken_at' => $date->toIso8601String(),
        ]);

        $response->assertStatus(201);

        $photo = $response->decodeResponseJson()['photo'];

        $this->assertEquals(Trip::find($trip['id'])->first()->photos->count(), 1);
        $this->assertEquals(Trip::find($trip['id'])->first()->photos->first()->name, $photo['name']);
        $this->assertEquals(Trip::find($trip['id'])->first()->photos->first()->taken_at, $date->setTimezone('UTC'));

        Storage::assertExists($user['id'].'/'.$trip['id'].'/'.$photo['name']);

        // test failure if exceeded number of allowed trips

        $plan = Plan::where('name', 'free')->first();
        $plan->allowed_photos_per_trip = 2;
        $plan->save();

        $file = UploadedFile::fake()->image('photo.jpg');

        $date = Carbon::now();
        $response = $this->withAuth($user)->json('post', '/api/photo/'.$trip['id'].'/new', [
            'photo' => $file,
            'taken_at' => $date->toIso8601String(),
        ])->assertStatus(201);

        $date = Carbon::now();
        $response = $this->withAuth($user)->json('post', '/api/photo/'.$trip['id'].'/new', [
            'photo' => $file,
            'taken_at' => $date->toIso8601String(),
        ]);
        $response->assertStatus(403)->assertJson(['message' => 'Exceeded number of allowed photos per trip.']);
    }

    public function testShow()
    {
        $user = $this->makeUser();

        $trip = $this->makeTrip($user);

        $photo = $this->makePhoto($user, $trip);

        $response = $this->withAuth($user)->json('get', '/api/photo/show/'.$photo['id'].'/');

        $response->assertStatus(200);
        $response->assertHeader('Content-type', 'image/jpeg');
    }

    public function testShowThumbnail()
    {
        $user = $this->makeUser();

        $trip = $this->makeTrip($user);

        $photo = $this->makePhoto($user, $trip);

        $response = $this->withAuth($user)->json('get', '/api/photo/showThumbnail/'.$photo['id'].'/');

        $response->assertStatus(200);
        $response->assertHeader('Content-type', 'image/jpeg');
    }
}
