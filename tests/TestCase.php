<?php

namespace Tests;

use Carbon\Carbon;
use App\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    protected function setUp(): void
    {
        parent::setUp();
        $this->seed();
    }

    protected function withAuth($user)
    {
        return $this->withHeaders(['Authorization' => 'Bearer '.$user['id'].'.'.$user['token']]);
    }

    protected function makeUser($id = 1, $name = 'test')
    {
        $response = $this->json('post', '/api/register', [
            'name' => $name.$id,
            'email' => 'test'.$id.'@yahoo.com',
            'password' => 'password123456',
            'password_confirmation' => 'password123456',
        ]);

        $user = $response->decodeResponseJson()['user'];
        User::where('id', $user['id'])->first()->verify();

        $response = $this->json('post', '/api/login', [
            'email' => 'test'.$id.'@yahoo.com',
            'password' => 'password123456',
        ]);

        return $response->decodeResponseJson()['user'];
    }

    protected function makeTrip($user, $location = 'China', $name = 'Best trip ever')
    {
        $response = $this->withAuth($user)->json('post', '/api/trip/new', [
            'trip' => ['location' => $location, 'name' => $name],
        ]);

        return $response->decodeResponseJson()['trip'];
    }

    protected function makePhoto($user, $trip)
    {
        $file = UploadedFile::fake()->image('photo.jpg');

        $response = $this->withAuth($user)->json('post', '/api/photo/'.$trip['id'].'/new', [
            'photo' => $file,
            'taken_at' => Carbon::now()->toIso8601String(),
        ]);

        return $response->decodeResponseJson()['photo'];
    }

    protected function makeDiary($user, $trip)
    {
        $response = $this->withAuth($user)->json('post', '/api/diary/'.$trip['id'].'/new', [
            'title' => '1 dec 1918',
            'body' => 'sadsadsadsaaaaaa',
            'taken_at' => Carbon::now()->toIso8601String(),
        ]);

        return $response->decodeResponseJson()['diary'];
    }
}
