Redirecting...

<script>
    window.onload = function(){
        
        if(navigator.userAgent.toLowerCase().indexOf("android") > -1){
            window.location.replace("travelcam://landing/{{$message}}");
        
            setTimeout(function() {
                window.location.replace("{{ $fallbackUrl }}");
            }, 2000);
        }else {
            window.location.replace("{{ $fallbackUrl }}");
        }
    };
</script>
