@extends('layouts.app')

@section('stylesheets')
@endsection

@section('title')
    Email verified
@endsection

@section('content')
    
    <div style="margin-top:40px" class="col-lg-6 col-md-12 col-lg-offset-3">
       <h1>Email successfully verified</h1>
       <p>You can now log into the Android app.</p>
    </div>

@endsection

@section('scripts')
@endsection
