@extends('layouts.app')

@section('stylesheets')
@endsection

@section('title')
    Enter PIN
@endsection

@section('content')
    
    <div style="margin-top:40px" class="col-lg-6 col-md-12 col-lg-offset-3">
        @if(session()->has('error'))
            <div class="alert alert-danger" role="alert">{{ session('error') }}</div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <form method="post" action="{{ action('WebController@authenticatePin', ['trip' => $trip['id']]) }}" >
            <div class="form-group">
                <label for="pin">PIN</label>
                <input type="password" name="pin" class="form-control" id="pin" placeholder="******">
            </div>

            <input type="hidden" name="tripId" value="{{ $trip['id'] }}">
            
            {{ csrf_field() }}

            <button type="submit" class="btn btn-primary">Enter PIN</button>
        </form>
    </div>

@endsection

@section('scripts')
@endsection
