<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <!-- Add metadata -->
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>TravelCam - The ultimate travel journal</title>
    
    <link rel="icon" href="{{asset('img/logo.svg')}}" type="image/svg">

    <!-- Font Awesome icons (free version)-->
    <script
      src="https://use.fontawesome.com/releases/v5.12.1/js/all.js"
      crossorigin="anonymous"
    ></script>
    <!-- Google fonts-->
    <link
      href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700"
      rel="stylesheet"
    />
    <link
      href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic"
      rel="stylesheet"
      type="text/css"
    />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{ asset("css/home.css") }}" rel="stylesheet" />
  </head>
  <body id="page-top">
    <!-- Navigation-->
    <nav
      class="navbar navbar-expand-lg navbar-light fixed-top py-3"
      id="mainNav"
    >
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">
          <img class="brand-image" src="{{asset('img/logo.svg')}}" width="35" height="35" />

          TravelCam

        </a>
        <button
          class="navbar-toggler navbar-toggler-right"
          type="button"
          data-toggle="collapse"
          data-target="#navbarResponsive"
          aria-controls="navbarResponsive"
          aria-expanded="false"
          aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto my-2 my-lg-0">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead">
      <div class="container h-100">
        <div
          class="row h-100 align-items-center justify-content-center text-center"
        >
          <div class="col-lg-10 align-self-end">
            <h1 class="text-uppercase text-white font-weight-bold">
              The ultimate travel journal app.
            </h1>
            <div class="col-lg-5 col-md-5" style="text-align: center; margin:auto;">
                <a href="https://play.google.com/store/apps/details?id=com.travelcam.app">
                  <img style="width:100%;" src="{{ asset("img/google-play-badge.png") }}" />
                </a>
            </div>
            <hr class="divider my-4" />
          </div>
          <div class="col-lg-8 align-self-baseline">
            <p class="font-weight-light mb-5 text-white">
              Manage all your vacation memories in one place.<br>
              Find inspiration and share your memories.
            </p>
            <a class="btn btn-primary btn-xl js-scroll-trigger" href="#about"
              >Find Out More</a
            >
          </div>
        </div>
      </div>
    </header>
    <!-- About section-->
    <section class="page-section bg-primary" id="about">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8 text-center">
            <h2 class="text-white mt-0">The all-in-one camera app for traveling.</h2>
            <hr class="divider light my-4" />
            <p class="text-white-75 mb-4">
              No more photo clutter.<br>
              No more transferring photo archives with loved ones.<br>
              Easily manage, store and share all of your memories.
            </p>
          </div>
        </div>
      </div>
    </section>
    <!-- Services section-->
    <section class="page-section" id="services">
      <div class="container">
        <h2 class="text-center mt-0">Free plan</h2>
        <hr class="divider my-4" />
        <div class="row">
          <div class="col-lg-3 col-md-6 text-center">
            <div class="mt-5">
              <i class="fas fa-4x fa-cloud text-primary mb-4"></i>
              <h3 class="h4 mb-2">Cloud storage</h3>
              <p class="text-muted mb-0">
                Free up your phone from vacation photos
              </p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="mt-5">
              <i class="fas fa-4x fa-image text-primary mb-4"></i>
              <h3 class="h4 mb-2">Original quality</h3>
              <p class="text-muted mb-0">
                Highest quality images are stored
              </p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="mt-5">
              <i class="fas fa-4x fa-globe text-primary mb-4"></i>
              <h3 class="h4 mb-2">Offline mode</h3>
              <p class="text-muted mb-0">
                You can still take pictures and make memories while offline
              </p>
            </div>
          </div>
          <div class="col-lg-3 col-md-6 text-center">
            <div class="mt-5">
              <i class="fas fa-4x fa-heart text-primary mb-4"></i>
              <h3 class="h4 mb-2">Free</h3>
              <p class="text-muted mb-0">
                Free for up to 5 trips (50 photos/trip)
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Portfolio section-->
    <section id="portfolio">
      <div class="container-fluid p-0">
        <div class="row no-gutters">
          @foreach($trips as $trip)
            <div class="col-lg-4 col-sm-6"">
              <a class="portfolio-box" href="{{ action('WebController@showTrip', ['trip' => $trip['id']]) }}"
                ><img
                  class="img-fluid"
                  src="{{ asset('img/thumbnails/'.$trip['thumbnail_id'].'.jpg') }}"
                  alt=""
                />
                <div class="portfolio-box-caption">
                  <div class="project-category text-white-50">{{ $trip['location'] }}</div>
                  <div class="project-name">{{ $trip['name'] }}</div>
                </div></a
              >
            </div>
          @endforeach
        </div>
      </div>
    </section>
    <!-- Call to action section-->
    <section class="page-section bg-dark text-white">
      <div class="container text-center">
        <h2 class="mb-4">Premium plan coming soon!</h2>
        <div class="col-lg-5 col-md-5" style="text-align: center; margin:auto;">
            <a href="https://play.google.com/store/apps/details?id=com.travelcam.app">
              <img style="width:100%;" src="{{ asset("img/google-play-badge.png") }}" />
            </a>
        </div>
      </div>
    </section>
    <!-- Contact section-->
    <section class="page-section" id="contact">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-8 text-center">
            <h2 class="mt-0">Contact</h2>
            <hr class="divider my-4" />
            <p class="text-muted mb-5">
              Shoot us an email with any problem/feedback you may have!
            </p>
          </div>
        </div>
        <div class="row">
          
          <div class="col-lg-4 ml-auto mr-auto text-center">
            <i class="fas fa-envelope fa-3x mb-3 text-muted" style="color:#40a35b !important;"></i
            ><a
              class="d-block"
              href="mailto:travelcam.app@gmail.com"
              >travelcam.app@gmail.com</a
            >
          </div>
        </div>
      </div>
    </section>
    <!-- Footer-->
    <footer class="bg-light py-5">
      <div class="container">
        <div class="small text-center text-muted">
          Copyright © TravelCam - built by <a href="https://www.linkedin.com/in/alin-dima-324796153/">Alin Dima</a>
        </div>
      </div>
    </footer>
    <!-- Bootstrap core JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
    <!-- Third party plugin JS-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.min.js"></script>
    <!-- Core theme JS-->
    <script src="{{ asset("js/home.js") }}"></script>
  </body>
</html>
