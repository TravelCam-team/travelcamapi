@extends('layouts.app')

@section('stylesheets')
@endsection

@section('title')
    Password changed
@endsection

@section('content')
    
    <div style="margin-top:40px" class="col-lg-6 col-md-12 col-lg-offset-3">
       <h1>Password was successfully changed</h1>
       <p>You can now log into the Android app with your new password.</p>
    </div>

@endsection

@section('scripts')
@endsection
