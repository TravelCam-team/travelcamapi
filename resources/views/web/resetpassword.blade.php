@extends('layouts.app')

@section('stylesheets')
@endsection

@section('title')
    Reset password
@endsection

@section('content')
    <div style="margin-top:40px" class="col-lg-6 col-md-12 col-lg-offset-3">
        <h1>Reset password</h1>
        <form class="form-horizontal" method="post" action="{{action('ResetPasswordController@postResetPassword')}}">
            <input type="hidden" name="token" value="{{$token}}"">
            {{ csrf_field() }}

            @if ($errors->has('password'))
                <div class="form-group has-error">
            @else
                <div class="form-group">
            @endif
                <label for="password" class="control-label">New Password</label>
                <input type="password" class="form-control" id="password" name="password">
                @if ($errors->has('password'))
                    <span class="help-block">{{$errors->first('password')}}</span>
                @endif
            </div>

            <div class="form-group">
                <label for="password_confirmation" class="control-label">Re-type Password</label>
                <input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-default">Change password</button>
            </div>
        </form>
    </div>

@endsection

@section('scripts')
@endsection
