@extends('layouts.app')

@section('stylesheets')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css">

    <style>
        .diary {
            width:400px;
            height:400px;
            max-height:400px;
            margin:0 auto;
            overflow: hidden !important;
        }

        .diary-link {
            color: black;
        }

        .diary-link:hover{
            text-decoration: none;
        }
    </style>
@endsection

@section('title')
    {{ $trip['name'] }}
@endsection

@section('navbar-collapse')
    <ul class="nav navbar-nav navbar-right">
        <div class="btn-group navbar-btn">
            <button type="button" style="margin-right:15px;" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Download <span class="caret"></span>
            </button>
            
            <ul class="dropdown-menu">
                <li>
                    <a href="javascript:;" onclick="document.getElementById('downloadAllForm').submit();">Download everything</a>
                    <form id="downloadAllForm" method="post" action="{{action('WebController@downloadAll', ['trip' => $trip['id']])}}">
                    {{ csrf_field() }}
                    </form>
                </li>
                <li>
                    <a href="javascript:;" onclick="document.getElementById('downloadPhotosForm').submit();">Download photos</a>
                    <form id="downloadPhotosForm" method="post" action="{{action('WebController@downloadPhotos', ['trip' => $trip['id']])}}">
                    {{ csrf_field() }}
                    </form>
                </li>
            </ul>
        </div>
    </ul>
@endsection

@section('content')
    <div class="row">
        <h4 style="margin-left:40px; margin-top:20px;">
            <img width="40" height="40" style="border-radius:100%; margin-right:10px; object-fit: cover;" src="{{ $user['profile_picture_permalink'] }}">
            {{ $user['name'] }}
        </h4>
    </div>
    
    <div class="row">
        <div class="page-header" style="margin:0 40px; margin-bottom: 30px">
            <h1>{{ $trip['name'] }} <small>{{ $trip['location'] }}</small></h1>
        </div>
    </div>
    
        @if($resources && $resources->count())
            <div class="row text-center text-lg-left">
                @foreach($resources as $res)
                    @if($res['body'])
                        <div class="col-lg-3 col-md-4 col-sm-6" style="margin-bottom:30px;">
                            <a class="diary-link" href="{{ action('WebController@showDiaryBody', ['diary' => $res['id']]) }}" data-gallery="lightbox" data-type="url" data-title="{{ $res['title'] }}">
                                <div class="img-fluid img-responsive panel panel-default text-left diary">
                                    <div class="panel-heading">{{ $res['title'] }}</div>
                                    <div class="panel-body">
                                        {{ $res['body'] }}
                                    </div>
                                </div>    
                            </a>
                        </div>
                    @else
                        <div class="col-lg-3 col-md-4 col-sm-6" style="margin-bottom:30px;">
                            <a href="{{ action('WebController@showPhoto', ['photo' => $res['id']]) }}" data-type="image" data-gallery="lightbox">
                                <img style="width:400px; height:400px; max-height:400px; object-fit:cover;" class="img-fluid img-thumbnail" src="{{ action('WebController@showThumbnail', ['photo' => $res['id']]) }}">
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>

            <div class="row text-center">
                {{ $resources->links() }}
            </div>

        @else
            <div class="row text-center text-lg-left">
                <h2>No memories from this trip</h2>
            </div>
        @endif

@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.min.js"></script>

    <script>
        $(document).on('click', '[data-gallery="lightbox"]', function(event) {
                event.preventDefault();
                $(this).ekkoLightbox({
                    {{-- alwaysShowClose: true, --}}
                    wrapping: false,
                });
            });
    </script>
@endsection
